# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        
        # Traverse tree in level-order
        
        # If "row" is a palindrome, then the tree is symmetric
        
        # Else it is not
        
        result = []
        
        if root is None:
            return True
        
        res = []            
        nodes = [root]
        while nodes:
            # Need to pre-create a "result" list, since each level needs to be a list 
            res.append( [ node.val for node in nodes] )
            next_nodes = [ ]
            
            # We are using these treenodes with val =-100 as placeholders in the end result, for the null branches of the tree.
            n = [ node for node in nodes if node.val is not -100]
            for node in n:
                if node.left:
                	next_nodes.append(node.left)
                else:
                    next_nodes.append(TreeNode(-100))
                    
                if node.right:
                    next_nodes.append(node.right)
                else:
                    next_nodes.append(TreeNode(-100))
                    
            # Check for palindrome here
            lenList = len(next_nodes)-1
            for i in range(lenList):
                print "{} and {}".format(next_nodes[i].val, next_nodes[lenList-i].val)
                if next_nodes[i].val != next_nodes[lenList-i].val:
                    return False
            
            nodes = next_nodes
        
        return True
