/**
 * Definition for TreeNode.
 * type TreeNode struct {
 *     Val int
 *     Left *ListNode
 *     Right *ListNode
 * }
 */
func lowestCommonAncestor(root, p, q *TreeNode) *TreeNode {
	lca := root
	for {
		if root == nil {
			return lca
		}

		if p.Val < root.Val && q.Val < root.Val {
			return lowestCommonAncestor(root.Left, p, q)
		} else if p.Val > root.Val && q.Val > root.Val {
			return lowestCommonAncestor(root.Right, p, q)
		} else {
			// if one is greater and one is less than, then root has to be the LCA.
			return root
		}
	}

}