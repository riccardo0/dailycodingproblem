# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def rightSideView(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        result = []
        
        if root is None:
            return []
        
        res = []            
        nodes = [root]
        while nodes:
            # Need to pre-create a "result" list, since each level needs to be a list 
            res.append( nodes[0].val ) # shamelessly stole this list comprehension from SO 
            next_nodes = [ ]
            for node in nodes:
                if node.right:
                    next_nodes.append(node.right)
                if node.left:
                    next_nodes.append(node.left)
            nodes = next_nodes
        
        return res
        