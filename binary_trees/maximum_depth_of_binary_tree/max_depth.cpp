/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int max(int a, int b){
        return a > b ? a : b; 
    }
    int maxDepthH(TreeNode *root, int height){
        // Get max depth of each child
        int maxLeft = 0;
        int maxRight = 0;
        
        // base case: we're at a dead end
        if (!root){
            return 0;
        }
        
        if (root->left){
            maxLeft = maxDepthH(root->left, height+1);
        } else {
            maxLeft = height;
        }
        if (root->right){
            maxRight = maxDepthH(root->right, height+1);
        } else {
            maxRight = height;
        }
        return max(maxLeft, maxRight);
    }
    
    int maxDepth(TreeNode* root) {
       return maxDepthH(root, 1);
    }
};
