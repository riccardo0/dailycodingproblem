/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isSameTree(p *TreeNode, q *TreeNode) bool {

	// this is ugly af, probably better way to check
	if p == nil {
		if q == nil {
			return true
		}
		return false
	} else if q == nil {
		if p == nil {
			return true
		}
		return false
	}

	return p.Val == q.Val &&
		isSameTree(p.Left, q.Left) &&
		isSameTree(p.Right, q.Right)
}