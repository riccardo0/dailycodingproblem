/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func postorderTraversal(root *TreeNode) []int {
    result := make([]int, 0)
    if root == nil {
        return result
    }
    
    // print left
    result = append(result, postorderTraversal(root.Left)...)
    
    // print right 
    result = append(result, postorderTraversal(root.Right)...)
    
    // add root to result
    result = append(result, root.Val)

    return result
}
