# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from collections import deque

class Solution(object):

    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        
        result = []
        
        if root is None:
            return []
        
        res = []            
        nodes = [root]
        while nodes:
            # Need to pre-create a "result" list, since each level needs to be a list 
            res.append( [ node.val for node in nodes] ) # shamelessly stole this list comprehension from SO 
            next_nodes = [ ]
            for node in nodes:
                if node.left:
                    next_nodes.append(node.left)
                if node.right:
                    next_nodes.append(node.right)
            nodes = next_nodes
        
        return res
            
