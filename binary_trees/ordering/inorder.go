/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func inorderTraversal(root *TreeNode) []int {
    result := make([]int, 0)
    if root == nil {
        return result
    }
    
    // print left
    result = append(result, inorderTraversal(root.Left)...)
    
    // add root to result
    result = append(result, root.Val)
    
    // print right 
    result = append(result, inorderTraversal(root.Right)...)

    return result
}
