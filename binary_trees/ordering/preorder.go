/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func preorderTraversal(root *TreeNode) []int {
    result := make([]int, 0)
    if root == nil {
        return result
    }
    
    // add root to result
    result = append(result, root.Val)
    
    // print left
    result = append(result, preorderTraversal(root.Left)...)
    
    // print right 
    result = append(result, preorderTraversal(root.Right)...)

    return result
}
