"""
# Definition for a Node.
class Node:
    def __init__(self, val, children):
        self.val = val
        self.children = children
"""
class Solution:
    def levelOrder(self, root: 'Node') -> List[List[int]]:
        result = []
        
        if root is None:
            return []
        
        res = []            
        nodes = [root]
        while nodes:
            # Need to pre-create a "result" list, since each level needs to be a list 
            res.append( [ node.val for node in nodes] ) # shamelessly stole this list comprehension from SO 
            next_nodes = [ ]
            for node in nodes:
                for child in node.children:
                    next_nodes.append(child)
            nodes = next_nodes
        
        return res