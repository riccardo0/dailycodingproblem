/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func removeNthFromEnd(head *ListNode, n int) *ListNode {
    savedHead := head
    // need to send the first pointer "N" nodes ahead, then send them both until we get to the end. The second
    // will now be the correct distance away.
    firstHead := head
    for i := 0; i < n; i++ {
        firstHead = firstHead.Next
    }
    
    secondHead := head 
    for firstHead != nil {
        secondHead = secondHead.Next
        firstHead = firstHead.Next
    }
    
    secondHead.Val = secondHead.Next.Val
    secondHead.Next = secondHead.Next.Next
    return savedHead
}
