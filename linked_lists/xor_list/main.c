#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

typedef struct _xor_list {
    unsigned long both; // prev ^ next pointers
    int val;
} xor_list;

bool hasNext(xor_list *node, xor_list *previous){
    //printf("hasNext: %d\n", ((unsigned long)node->both ^ (unsigned long)previous) != 0);
    if(node == NULL){
        return false;
    }

    // printf("node addr: %p\n", node);
    return (((unsigned long)node->both ^ (unsigned long)previous) != 0);
}


// To add:
// traverse through list, have to set next pointer equal to prevPtr ^ bothPtr 
// if prevPtr ^ bothPtr == 0 
//      we're at the end of the list and we can tack on a new node next (node->both = prevPtr ^ newNodePtr)
void add(xor_list *l, int val){
    xor_list *prev = NULL;
    xor_list *cur = l;
    int i = 0;
    for (;;) {
        // how do we handle the first case where we just have the head?
        if (!hasNext(cur, prev)) {
            printf("Base case, adding new node here\n");
            // create new node
            xor_list *newNode = malloc(sizeof(xor_list));
            newNode->val = val;
            newNode->both = (unsigned long)cur; // "prev" ^ 0
            // need to update current's pointer to be prev ^ next
            cur->both = (unsigned long)newNode ^ (unsigned long)prev; // update reference to include the new next node
            printf("Adding node at index: %d\n", i++);
            return;
        } else {
            // Save prev (cur => prev)
            // Set current to next (next = cur->both ^ prev)
            // printf("iterating; currVal: %d\n", cur->val);
            xor_list *prevTmp = cur;
            cur = (xor_list *)((unsigned long)cur->both ^ (unsigned long)prev);
            prev = prevTmp;
        }

        i++;
    }
}

// get element at index _index_
int get(xor_list *l, int index){
    if (l == NULL){
        return -1;
    }

    int i;
    int ret = l->val;
    xor_list *prev = NULL;
    xor_list *cur = l;
    for (i = 0; i < index; i++) {
        xor_list *prevTmp = cur;
        cur = (xor_list *)((unsigned long)cur->both ^ (unsigned long)prev);
        prev = prevTmp;
        ret = cur->val;
    }
    return ret;
}

int main(){
    printf("Hello world\n");
    xor_list *head = malloc(sizeof(xor_list));
    head->both = 0;
    head->val = 1;
    
    // Add vals 2, 3, 4 to list
    add(head, 2);
    add(head, 3);
    add(head, 4);

    int val1 = get(head, 0);
    int val2 = get(head, 1);
    int val3 = get(head, 2);
    int val4 = get(head, 3);

    printf("Val1: %d\nVal2: %d\nVal3: %d\nVal4: %d\n", val1, val2, val3, val4);
    
    assert(val1 == 1);
    assert(val2 == 2);
    assert(val3 == 3);
    assert(val4 == 4);
    
    return 0;
}