import "fmt"

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func hasCycle(head *ListNode) bool {
	slow := head
	fast := head
	for {
		if slow == nil || fast == nil {
			return false
		}
		if slow.Next == nil {
			return false
		}
		if fast.Next == nil || fast.Next.Next == nil {
			return false
		}
		slow = slow.Next
		fast = fast.Next.Next
		// if these two ever meet, then we have a cycle.
		if slow == fast {
			fmt.Printf("Found cycle; %+v == %+v\n", slow, fast)
			return true
		}
	}
	return false
}