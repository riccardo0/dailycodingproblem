/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */

// This is incredibly ugly tbh
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	res := &ListNode{}
	resItr := res
	listInitialized := false
	carry := 0
	for !(l1 == nil && l2 == nil) {
		val1, val2 := 0, 0
		if l1 != nil {
			val1 = l1.Val
			l1 = l1.Next
		}
		if l2 != nil {
			val2 = l2.Val
			l2 = l2.Next
		}
		digitResult := val1 + val2 + carry
		if digitResult > 9 {
			carry = 1
			digitResult = digitResult % 10
		} else {
			carry = 0
		}

		if !listInitialized {
			res = &ListNode{
				Val: digitResult,
			}
			resItr = res
			listInitialized = true
		} else {
			resItr.Next = &ListNode{
				Val: digitResult,
			}
			resItr = resItr.Next
		}

	}

	// TODO: Handle extra carry, and lists of diff length
	if carry == 1 {
		resItr.Next = &ListNode{
			Val: 1,
		}
	}

	return res
}