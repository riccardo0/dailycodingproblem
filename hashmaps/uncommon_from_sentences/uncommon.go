import "strings"

func uncommonFromSentences(A string, B string) []string {
	// store words that exist in a map of strings->struct{} for existence
	wordExistenceMap := make(map[string]int)
	uniqueWords := []string{}

	firstSentence := strings.Split(A, " ")
	secondSentence := strings.Split(B, " ")

	// add all words from first sentence
	for _, word := range firstSentence {
		if _, ok := wordExistenceMap[word]; !ok {
			wordExistenceMap[word] = 1
		} else {
			wordExistenceMap[word]++
		}
	}

	// all all words from second sentence
	for _, word := range secondSentence {
		if _, ok := wordExistenceMap[word]; !ok {
			wordExistenceMap[word] = 1
		} else {
			wordExistenceMap[word]++
		}
	}

	// any words with count=1 existed only once, in 1 of the sentences.
	for word, count := range wordExistenceMap {
		if count == 1 {
			uniqueWords = append(uniqueWords, word)
		}
	}

	return uniqueWords

}