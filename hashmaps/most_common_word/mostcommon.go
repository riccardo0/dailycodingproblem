import (
	"fmt"
	"regexp"
	"strings"
)

var reg = regexp.MustCompile("[^a-zA-Z0-9]+")

func mostCommonWord(paragraph string, banned []string) string {
	bannedMap := make(map[string]struct{})
	wordMap := make(map[string]int)

	// sanitize the string to remove non-alphanum chars and replace with space
	paragraph = reg.ReplaceAllString(paragraph, " ")

	paragraphAsStrSlice := strings.Fields(paragraph)
	for _, bannedWord := range banned {
		bannedMap[bannedWord] = struct{}{}
	}
	for _, word := range paragraphAsStrSlice {
		word = strings.ToLower(word)
		// skip banned words
		if _, ok := bannedMap[word]; ok {
			continue
		}

		// if word is not banned, check if it exists.
		if _, ok := wordMap[word]; !ok {
			fmt.Printf("Word (%s) not found, adding to map\n", word)
			wordMap[word] = 0
		}
		fmt.Printf("Incrementing word (%s) count by 1\n", word)
		wordMap[word]++
	}

	maxCount := 0
	maxWord := ""
	for word, wordCount := range wordMap {
		if wordCount > maxCount {
			maxCount = wordCount
			maxWord = word
		}
	}

	return maxWord
}