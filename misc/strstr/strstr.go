import "fmt"

func strStr(haystack string, needle string) int {
	// make a sliding window of length = len(needle), and keep sliding it across the word to see if we find our match.

	if needle == "" || needle == haystack {
		return 0
	}

	// If wordLen is 5 and windowlen is 2, we'll need to slide it 3 times
	wordLen := len(haystack)
	windowLen := len(needle)
	lenToCheck := wordLen - windowLen

	for i := 0; i < lenToCheck+1; i++ {
		word := haystack[i : i+windowLen]
		fmt.Printf("Checking word: %s\n", word)
		if word == needle {
			return i
		}
	}

	return -1
}