import "math"

// Sieve of eratosthenes (sp?)
// if I recall (going to check before I submit):
// formula is to mark all primes starting from 2 and then
// iterate over list and see if each val is prime
func basicSieve(n int) []bool {
	res := make([]bool, n+1)
	for i := 2; i <= int(math.Sqrt(float64(n))); i++ {
		for j := int(math.Pow(float64(i), 2)); j <= n; j += i {
			//fmt.Printf("%d is prime\n", j)
			//fmt.Printf("Loop i:[%d] j:[%d]\n", i, j)
			res[j] = true
		}
	}
	return res
}

func countPrimes(n int) int {
	count := 0
	sieve := basicSieve(n)
	for i := 2; i < n; i++ {
		// !ok means that it is not in the sieve, so it must be prime.
		if !sieve[i] {
			count++
		}
	}
	return count
}