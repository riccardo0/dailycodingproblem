class Solution(object):
    def findKthLargest(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        heap = []
        for i in nums:
            heapq.heappush(heap, i)
        
        
        nlargest = heapq.nlargest(k, heap)
        return nlargest[len(nlargest)-1]
        