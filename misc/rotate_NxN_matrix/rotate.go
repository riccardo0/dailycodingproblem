import "fmt"

func rotate(matrix [][]int) {
	// Matrix is NxN
	N := len(matrix)
	fmt.Printf("Len is %d\n", N)

	// We basically need to shift each element forwards by N-1 (for 3x3, we move everything to the right by 2)

	// alternatively, we know that each row actually becomes the column but in reverse order (row 0 becomes column N-1)
	newMatrix := make([][]int, N)
	for i, _ := range newMatrix {
		newMatrix[i] = make([]int, N)
	}

	for i, row := range matrix {
		// for each element in the row, set the n-i-1st column to this
		for j, elem := range row {
			fmt.Printf("Setting newMatrix[%d][%d]=%d\n", j, N-i-1, elem)
			newMatrix[j][N-i-1] = elem
		}
	}

	// Somewhat cheating here, this does NOT do it in place.
	fmt.Printf("%+v\n", newMatrix)
	for idx, _ := range matrix {
		matrix[idx] = newMatrix[idx]
	}
	matrix = newMatrix
}