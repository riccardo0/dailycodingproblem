class Solution(object):
    parenMap = {}
    parenMap[")"]= "("
    parenMap["}"]= "{"
    parenMap["]"]= "["
    
    parenOpen = ["(", "{", "["]
    
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        
        stack = []
        
        for ch in s:
            if ch in self.parenOpen:
                print "Appending ", ch, " to stack"
                stack.append(ch)
            else:
                print "Char ", ch, " is not a beginning paren"
                if len(stack) is 0:
                    return False
                
                # If the opposite paren to the current is what is on the stack, we're good
                if self.parenMap[ch] == stack.pop():
                    continue
                else:
                    return False
        
        if len(stack) is 0:
            return True
        return False