func missingNumber(nums []int) int {
    partialSum := 0
    largest := len(nums) // since we know we have 0-N minus 1 num in the array, we have N+1 elements
    
    for _, num := range nums {
        partialSum += num
    }
    
    // sum of 1-n = n(n+1)/2, so we can find missing number by subtracting our sum from the formula
    return (largest * (largest+1) / 2.0) - partialSum
}